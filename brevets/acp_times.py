"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    # The ranges of each control and their max speed
    control_lengths_max = [(200, 34), (200, 32), (200, 30), (400, 28)]

    # If the control is longer than the brevet, control should be redefined as brevet length
    # (No controls past the end)
    if (control_dist_km > brevet_dist_km):
       control_dist_km = brevet_dist_km

    time_from_start = 0
    temp_control_dist = control_dist_km
    i = 0
    control_range = 200
    
    # Loop through the control ranges until we reach the range control_dist_km is within
    # Adding the control/max for each control range passed through
    while (temp_control_dist > control_range):
       control, max = control_lengths_max[i]
       time_from_start += control/max
       temp_control_dist -= control
       i += 1
       if(i == 3):
          control_range = 400

    # Add the final control/max using the leftover temp_control value
    control, max = control_lengths_max[i]
    time_from_start += temp_control_dist/max

    # Convert decimal to hours and minutes
    temp_time = time_from_start
    hour = 0
    minute = 0

    while(temp_time > 1):
        temp_time -= 1
        hour += 1

    minute = round((time_from_start - hour) * 60)
    
    # Format the date & time string
    open_time = arrow.get(brevet_start_time)
    open_time = open_time.shift(hours=hour, minutes=minute)
    return open_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    # The ranges of each control and their minimum speeds
    control_lengths_min = [(200, 15), (200, 15), (200, 15), (400, 11.428)]
    # The maximum time for each brevet length
    cutoff_times = {200: 13.5, 300: 20, 400: 27, 600: 40, 1000: 75}

    # If the control is at or past the end, use cutoff time for close_time
    if (control_dist_km >= brevet_dist_km):
       cutoff = cutoff_times[brevet_dist_km]
       close_time = arrow.get(brevet_start_time)
       close_time = close_time.shift(hours=cutoff)
       return close_time.isoformat()
   
    # If the control is 0, close time is 1 hour after start
    if (control_dist_km == 0):
      close_time = arrow.get(brevet_start_time)
      close_time = close_time.shift(hours=1)
      return close_time.isoformat()

    time_from_start = 0
    temp_control_dist = control_dist_km
    i = 0
    control_range = 200
    
    # Loop through the control ranges until we reach the range *control_dist_km* is within
    # Adding the control/min for each control range passed through
    while (temp_control_dist > control_range):
       control, min = control_lengths_min[i]
       time_from_start += control/min
       temp_control_dist -= control
       i += 1
       if(i == 3):
          control_range = 400

    # Add the final control/min using the leftover *temp_control_dist* value
    control, min = control_lengths_min[i]
    time_from_start += temp_control_dist/min

    # Convert decimal to hours and minutes
    temp_time = time_from_start
    hour = 0
    minute = 0

    while(temp_time > 1):
        temp_time -= 1
        hour += 1

    minute = round((time_from_start - hour) * 60)

    # Format the date & time string
    close_time = arrow.get(brevet_start_time)
    close_time = close_time.shift(hours=hour, minutes=minute)
    return close_time.isoformat()
