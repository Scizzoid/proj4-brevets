import acp_times

# All times the functions are tested against were taken from rusa.org/octime_acp.html using their calculator

# Test Edges (opening times)
def test_opening_edges():

    # Opening time at start is start time
    assert acp_times.open_time(0, 1000, "2021-11-05T00:00:00+00:00") == "2021-11-05T00:00:00+00:00"

    assert acp_times.open_time(200, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-01T05:53:00+00:00"
    assert acp_times.open_time(400, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-01T12:08:00+00:00"
    assert acp_times.open_time(600, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-01T18:48:00+00:00"
    assert acp_times.open_time(1000, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-02T09:05:00+00:00"

    # Past the limit should use the limit instead
    assert acp_times.open_time(1500, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-02T09:05:00+00:00"

# Test Edges (closing times)
def test_closing_edges():

    # Closing time at start is one hour past start time
    assert acp_times.close_time(0, 1000, "2021-11-05T00:00:00+00:00") == "2021-11-05T01:00:00+00:00"

    assert acp_times.close_time(200, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-01T13:20:00+00:00"
    assert acp_times.close_time(400, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-02T02:40:00+00:00"
    assert acp_times.close_time(600, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-02T16:00:00+00:00"
    assert acp_times.close_time(1000, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-04T03:00:00+00:00"

    # Past the limit should use the limit instead
    assert acp_times.close_time(1500, 1000, "2021-01-01T00:00:00+00:00") == "2021-01-04T03:00:00+00:00"

# Test Return Type (opening times)
def test_opening_return():

    # Should return a string
    assert type(acp_times.open_time(50, 200, "2021-11-05T00:00:00+00:00")) == str

# Test Return Type (closing times)
def test_closing_return():

    # Should return a string
    assert type(acp_times.close_time(100, 300, "2021-11-05T00:00:00+00:00")) == str

def test_input_data_type():

    # Does it work with floats?
    assert acp_times.open_time(200.0, 400.0, "2021-01-01T00:00:00+00:00") == "2021-01-01T05:53:00+00:00"
    assert acp_times.close_time(200.0, 400.0, "2021-01-01T00:00:00+00:00") == "2021-01-01T13:20:00+00:00"

